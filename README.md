# CloudinaryUrl

Generate signed urls ready to upload a file to [cloudinary](http://cloudinary.com/).

## About Cloudinary URL uploads

To upload an image to cloudinary requires a signed URL unless your cloudinary account has been set to accept unsigned uploads.

To upload in image to cloudinary requires a POST request to a url with a few manditory form encoded parameters including a signature. See the slightly cryptic [docs](http://cloudinary.com/documentation/upload_images#request_authentication).

## Generating the signature

1. taking the required form parameters
```
    api_key=abc
    timestamp=12345678910
    public_id=demoimage
```

2. turning them into query stringified version
```
    api_key=123&public_id='fish'&timestamp=1234678910
```

3. concating your secret key (SECRET) given by cloudinary to the end
```
    api_key=123&public_id='fish'&timestamp=1234678910SECRET
```

4. The signature is a sha1 hex digest of this
```
    a97a90c79a07039d9097af0a987fa29861fafaa987
```

## Using the library

```javascript
    var CloudinaryURl = require('cloudinary-url');

    var urlGenerator = new CloudinaryUrl('apiKey', 'apiSecret', 'bucketName');

    cloudinaryData = urlGenerator.sign({public_id: 'fish'})

    console.log(cloudinaryData.url)
    // https://api.cloudinary.com/v1_1/bucketName/image/upload

    console.log(cloudinaryData.params)
    /*
      {
        api_key: 'apiKey',
        public_id: 'fish',
        timestamp: '12345678910',
        signature: 'a97a90c79a07039d9097af0a987fa29861fafaa987'
      }
    */
```

These details can be used to make a request using your favourite HTTP library. I'm rather partial to [superagent](https://www.npmjs.org/package/superagent) for node.

```javascript
    var request = require('superagent'),

    request
      .post(cloudinaryData.url)
      .type('form')
      .field('signature', cloudinaryData.params.signature)
      .field('public_id', cloudinaryData.params.public_id)
      .field('api_key', cloudinaryData.params.api_key)
      .field('timestamp', cloudinaryData.params.timestamp)
      .attach('file', '/path/to/file.jpg')
      .end(function(err, res){
        // do something with response
      })
```

MIT LICENSE ETC ETC BOOM!
